import * as fs from 'fs-extra';
import * as os from "os";
import * as _ from 'lodash';
import {CronJob} from 'cron';

import {get} from "./api";

type Config = {configFilePath, configFileName, name, maxConcurrentJobs?, active?};

let config: Config = {
    configFilePath: `${os.homedir()}/.config/flusso/`,
    configFileName: 'config.json',
    name: ''
};

export const fullPath = `${config.configFilePath}${config.configFileName}`;

export function getConfig() {
    return config;
}

export function configureRunner(runnerConfig) {
    config = {...config, ...runnerConfig};
}

export function readRunnerConfig() {
    return fs.readJson(fullPath);
}

export async function writeRunnerConfig(content) {
    await fs.ensureDir(config.configFilePath);
    await fs.writeFileSync(fullPath, JSON.stringify(content, null, 4));
}

async function updateConfig() {
    let newConfig;

    try {
        newConfig = await get(`/runners/${config.name}`);
    } catch (_e) {}

    if (_.isEmpty(newConfig)) {
        return null;
    }

    configureRunner({active: newConfig.active, maxConcurrentJobs: newConfig.maxConcurrentJobs});
    writeRunnerConfig(getConfig());
}

export function runnerConfigCron() {
    const runnerConfigCron = new CronJob('*/5 * * * * *', updateConfig);

    runnerConfigCron.start();
}