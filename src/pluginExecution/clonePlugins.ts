import * as fs from 'fs-extra';
import {spawnSync} from 'child_process';

import {pushLogs} from "../jobExecution/executeJob";

const baseFolder = '/tmp/flusso/plugins';

type GitConfig = {type: string, sshUrl: string, username: string, token: string};
type Params = {job: object, name: string, version: string, path: string, git: GitConfig};

async function clonePlugin({job, name, version, path, git}: Params) {
    const {type = '', sshUrl = '', username = '', token = ''} = git;

    if (type !== 'gitlab') {
        return null;
    }

    const properUrl = sshUrl.replace('git@', '').replace(':', '/');
    const clone = spawnSync(`git clone https://${username}:${token}@${properUrl} -b ${name} ${path}`, {cwd: path, shell: true});

    clone.output && clone.output.toString() && await pushLogs(clone.output.toString(), job);

    if (version !== 'latest') {
        const checkout = spawnSync(`cd ${path} ; git checkout ${version}`);

        checkout.output && checkout.output.toString() && await pushLogs(checkout.output.toString(), job);
    }
}

export async function clonePlugins(job): Promise<{_id: string, path: string}[]> {
    let paths: {_id: string, path: string}[] = [];

    for (let {name, version, git, _id} of job.plugins) {
        if (!version) {
            version = 'latest';
        }
        const path = `${baseFolder}/${name}@${version}`;

        const exists = version !== 'latest' && await fs.pathExists(path);

        if (exists) {
            paths.push({_id, path});
            continue;
        }

        if (version === 'latest') {
            await fs.remove(path);
        }

        await fs.ensureDir(path);

        try {
            await clonePlugin({job, name, version, path, git});
            paths.push({_id, path});
        } catch (error) {
            console.log(error);
        }
    }

    return paths;
}
