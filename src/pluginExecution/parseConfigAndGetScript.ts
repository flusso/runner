import * as fs from 'fs-extra';
import * as yaml from 'js-yaml';
import * as _ from 'lodash';

export async function parseConfigAndGetScript(plugins, pluginIdsAndPaths) {
    let configs: any[] = [];

    for (const {_id, path} of pluginIdsAndPaths) {
        const configPath = `${path}/config.yml`;
        try {
            const fileContent = await fs.readFile(configPath, 'utf-8');

            const {title, entryPoint, envVariables} = yaml.safeLoad(fileContent);
            const {variables = {}} = _.find(plugins, {_id}) || {};

            configs.push({path, name: path.split('/').pop(), config : {title, entryPoint, envVariables, variables}});
        } catch (e) {
            throw new Error(`Error parsing ${configPath}`)
        }
    }

    return configs;
}
