import * as _ from 'lodash';
import {CronJob} from 'cron';

import {getConfig} from '../config';
import {get} from "../api";
import {killProcess} from "../jobExecution/processes";

export function runActions(actions) {
    for (const {action} of actions) {
        if (action.action === 'cancelJob') {
            killProcess(action.jobId);
        }
    }
}

async function runnerActions() {
	let actions;

	try {
        actions = await get(`/getRunnerActions/${getConfig().name}`);
    } catch (_e) {}

    if (!_.size(actions)) { return; }

    await runActions(actions);
}

export function runnerActionsCron() {
    const runnerActionsCron = new CronJob('* * * * * *', runnerActions);

    runnerActionsCron.start();
}
