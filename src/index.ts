import {configureRunner, fullPath, readRunnerConfig, runnerConfigCron} from "./config";
import {configureApi} from "./api";
import {jobCron} from "./jobExecution/jobCron";
import {runnerActionsCron} from "./runnerActions/runnerActionsCron";

async function main() {
    try {
        const runnerConfig = await readRunnerConfig();

        configureRunner(runnerConfig);
        configureApi(runnerConfig);
        jobCron();
        runnerActionsCron();
        runnerConfigCron();
    } catch(e) {
        return console.error(`[!] unable to open the config file at ${fullPath}!`);
    }
}

main().then();
