import * as Docker from 'dockerode';

let processes = {};

export function addProcess(jobId, containerId, worker) {
    processes[jobId] = {containerId, worker};
}

export async function killProcess(jobId) {
    if (processes[jobId]?.worker) {
        processes[jobId].worker.terminate();
    }
    if (processes[jobId]?.containerId) {
        const docker = new Docker();

        await docker.getContainer(processes[jobId].containerId).kill();
    }
}
