import * as _ from 'lodash';
import {ChildProcess, spawn} from 'child_process';

import {finishJob, pushLogs} from "./executeJob";

function getEnvVariables({_id, pipelineId}, plugins) {
	let variables = process.env;

	for (const {config} of plugins) {
		variables = {...variables, ...config.envVariables};
	}

	variables.PIPELINE_ID = pipelineId;
	variables.JOB_ID = _id;

	return variables;
}


function getPluginEntryPoints(plugin): string {
	let scripts = '';

	const {config: {variables = {}}} = plugin;

	let formattedVariables = '';
	for (const variable in variables) {
		formattedVariables += `${variable}="${variables[variable]}" `
	}

	scripts += `cd ${plugin.path};\n`;
	scripts += `${formattedVariables} ${plugin.config.entryPoint};\n`;
	scripts += `cd ${__dirname}\n`;

	return scripts;
}

function getScriptWithPlugins(job, plugins) {
	let splittedLines = (job.scripts || '').split('\n');
	const startPluginRex = /^#\s*flusso\.startPlugin (\{".+)/

	for (const lineIndex in splittedLines) {
		const line = splittedLines[lineIndex];
		const match = line.match(startPluginRex);
		if (match) {
			try {
				const {name, version = 'latest'} = JSON.parse(match[1]);
				const plugin = _.find(plugins, {name: `${name}@${version}`});

				splittedLines[lineIndex] = getPluginEntryPoints(plugin);

			} catch (_e) {}
		}
	}

	return splittedLines.join('\n');
}

export async function executeOnShell({job, plugins}) {
	return new Promise(async resolve => {
		const scriptWithPlugins = getScriptWithPlugins(job, plugins);
		const shell = job.shell || 'bash';

		const process: ChildProcess = spawn(shell, ['-c', scriptWithPlugins], {env: getEnvVariables(job, plugins)});

		process.stdout && process.stdout.on('data', async data => pushLogs(data.toString(), job));
		process.stderr && process.stderr.on('data', async data => {
			await pushLogs(data.toString(), job);
			finishJob(job, 1);
		});

		process.on('close', code => {
			finishJob(job, code);
			resolve();
		});

		process.on('exit', code => {
			finishJob(job, code);
			resolve();
		});
	});
}
