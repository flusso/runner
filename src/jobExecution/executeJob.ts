import * as _ from 'lodash';
import {workerData, parentPort} from 'worker_threads';

import {configureApi, put} from "../api";
import {readRunnerConfig} from "../config";
import {executeOnDocker} from './executeOnDocker';
import {executeOnShell} from './executeOnShell';
import {clonePlugins} from "../pluginExecution/clonePlugins";
import {parseConfigAndGetScript} from "../pluginExecution/parseConfigAndGetScript";

export async function pushLogs(status, job) {
	return put(`/jobs/${job._id}`, {$push: {logs: status}});
}

export function finishJob(job, code?) {
	parentPort?.postMessage({action: 'jobFinished', jobId: job._id, code});
}

function addContainer(job, containerId) {
	parentPort?.postMessage({action: 'addContainer', jobId: job._id, containerId});
}

export async function executeJob(job) {
	if (_.isEmpty(job)) {
		return;
	}

	configureApi(await readRunnerConfig());

	const pluginIdsAndPaths = await clonePlugins(job);
	const plugins = await parseConfigAndGetScript(job.plugins, pluginIdsAndPaths);

	if (job.image) {
		const container = await executeOnDocker({job, plugins});
		container && addContainer(job, container.id);
	} else {
		await executeOnShell({job, plugins});
	}
}

executeJob(workerData);
