import * as _ from 'lodash';
import * as Docker from 'dockerode';

import {finishJob, pushLogs} from './executeJob';

async function onFinished(job, container, code = 0) {
	finishJob(job, code);
	container && container.remove();
}

async function onError(err, job, container) {
	await pushLogs(err.toString(), job);

	onFinished(job, container, 1);
}

function getPluginsFolders(job, plugins): string[] {
	return _.map(plugins, ({name, path}) => `${path}:/flusso/${job._id}/plugins/${name}`)
}

function getPluginEntryPoints(job, plugin): string {
	let scripts = '';

	const {config: {variables = {}}} = plugin;

	let formattedVariables = '';
	for (const variable in variables) {
		formattedVariables += `${variable}="${variables[variable]}" `
	}

	scripts += `cd /flusso/${job._id}/plugins/${plugin.name};\n`;
	scripts += `${formattedVariables} ${plugin.config.entryPoint};\n`;
	scripts += `cd /flusso/${job._id}/;\n`;

	return scripts;
}

function getScriptWithPlugins(job, plugins) {
	let splittedLines = (job.scripts || '').split('\n');
	const startPluginRex = /^#\s*flusso\.startPlugin (\{".+)/

	for (const lineIndex in splittedLines) {
		const line = splittedLines[lineIndex];
		const match = line.match(startPluginRex);
		if (match) {
			try {
				const {name, version = 'latest'} = JSON.parse(match[1]);
				const plugin = _.find(plugins, {name: `${name}@${version}`});

				splittedLines[lineIndex] = getPluginEntryPoints(job, plugin);

			} catch (_e) {}
		}
	}

	return splittedLines.join('\n');
}

function getScripts(job, plugins): string[] {
	const scriptWithPlugins = getScriptWithPlugins(job, plugins);
	const shell = job.shell || 'bash';

	return [shell, '-c', scriptWithPlugins];
}

function getPluginsEnvVariables(plugins): string[] {
	let envVariables: string[] = [];

	for (const {config} of plugins) {
		envVariables.push(..._.map(config.envVariables, (value, key) => `${key}="${value}"`));
	}

	return envVariables;
}

function getEnvVariables({_id, pipelineId}, plugins) {
	return [...getPluginsEnvVariables(plugins), `PIPELINE_ID=${pipelineId}`, `JOB_ID=${_id}`];
}

function getHostConfig(job, plugins) {
	return {
		Binds: [...getPluginsFolders(job, plugins), `${__dirname}/../../:/flusso`]
	};
}

function pullImage(docker, job) {
	return new Promise((resolve, reject) => {
		return docker.pull(job.image, (err, stream) => {
			if (err) {
				return reject(err);
			}

			docker.modem.followProgress(stream, () => resolve(), output => pushLogs(output && output.status, job));
		});
	});
}

export async function executeOnDocker({job, plugins}) {
	const {_id, image} = job;
	let container;

	try {
		const docker = new Docker();

		await pullImage(docker, job);

		container = await docker.createContainer({
			Image: image,
			HostConfig: getHostConfig(job, plugins),
			Env: getEnvVariables(job, plugins),
			Cmd: getScripts(job, plugins),
			WorkingDir: `/flusso/${_id}`,
			Entrypoint: [''],
			Tty: true
		});

		container.attach({stream: true, stdout: true, stderr: true}, (err, stream) => {
			if (err) {
				onError(err, job, container)
			}

			stream.on('data', chunk => pushLogs(chunk.toString(), job));
			stream.on('error', err => onError(err, job, container));
			stream.on('end', () => onFinished(job, container));
		});

		await container.start();

		return container;
	} catch (e) {
		onError(e, job, container);
	}
}
