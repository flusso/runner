import * as _ from 'lodash';
import {CronJob} from 'cron';
import {Worker} from "worker_threads";

import {getConfig} from '../config';
import {get, put} from "../api";
import {addProcess} from "./processes";

let runningJobs = 0;

export function runJob(job) {
    runningJobs++;
    const worker: Worker = new Worker(__dirname + '/executeJob.js', {workerData: job});

    worker.on('message', async message => {
        if (message.action === 'jobFinished') {
            await put(`/finishJob/${message.jobId}`, {exitCode: message.code});
            runningJobs--;
        } else if (message.action === 'addContainer') {
            addProcess(message.jobId, message.containerId, worker);
        }
    });
}

async function getJobs() {
    const config = getConfig();

	if (config.active === false || runningJobs >= (config.maxConcurrentJobs || 1)) { return; }
	let job;

	try {
        job = await get(`/getAndStartJob/${config.name}`);
    } catch (_e) {}

    if (!_.get(job, '_id')) {
        return;
    }

    await runJob(job);
}

export function jobCron() {
    const jobCron = new CronJob('* * * * * *', getJobs);

    jobCron.start();
}
