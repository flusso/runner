import * as _ from 'lodash';
import * as qs from 'qs';
import axios from 'axios';

let instance;

export function configureApi({backendUrl, runnerRegistrationToken}) {
    instance = axios.create({baseURL: backendUrl, timeout: 1000, headers: {Authentication: runnerRegistrationToken, 'Content-Type': 'application/json'}});
}

async function safeCall(unsafeCall) {
    try {
        const {data} = await unsafeCall();

        return data;
    } catch (e) {
        const errorMessage = e?.response?.data?.message ?? e;
        console.error(errorMessage);
        return null;
    }
}

export async function plainPost(url, body = {}) {
    return safeCall(() => axios.post(url, body));
}

export function get(path, query?) {
    const url = _.isEmpty(query) ? path : `${path}?${qs.stringify(query)}`;

    return safeCall(() => instance.get(url));
}

export async function post(url, body = {}) {
    return safeCall(() => instance.post(url, body));
}

export async function put(url, body = {}) {
    return safeCall(() => instance.put(url, body));
}
