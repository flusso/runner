import * as inquirer from 'inquirer';

import {fullPath, readRunnerConfig, writeRunnerConfig} from './config';
import {plainPost} from "./api";

async function register() {
    try {
        const configFile = await readRunnerConfig();

        if (configFile) {
            console.log(`[!] It looks like you have already configured this runner!; you can find the config file at ${fullPath}`);

            return;
        }
    } catch (e) {}

    const answers = await inquirer.prompt([
        {message: 'Enter the backend\'s URL', name: 'backendUrl', type: 'text'},
        {message: 'Enter the `runnerRegistrationToken`', name: 'runnerRegistrationToken', type: 'text'},
        {message: 'Enter the name of the runner', name: 'name', type: 'text'},
        {message: 'Enter the tags for this runner (comma separated)', name: 'tags', type: 'text'},
    ]);

    let {backendUrl, runnerRegistrationToken, name, tags = ''} = answers;

    tags = tags.split(',').map(tag => tag.trim());
    const formattedBackendUrl = backendUrl.replace(/\/$/, '');
    const body = {runnerRegistrationToken, runner: {name, tags}};
    const responseFromBackendUrl = await plainPost(`${formattedBackendUrl}/runners`, body);

    try {
        if (responseFromBackendUrl) {
            await writeRunnerConfig(answers);
            console.log('[+] Runner registered successfully!');
        } else {
            console.error(`[!] It looks like I cannot register the runner ${name} on ${backendUrl}!!`);
        }
    } catch (e) {
        console.error(`[!] ${e}`);
    }
}

register();
