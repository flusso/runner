#!/usr/bin/env bash
set -e

command_exists() {
  command -v "$1" > /dev/null 2>&1
}

install() {
  # we are working on debian-like stuff
  if command_exists "apt-get"; then
    echo "✔️ Installing the dependencies"

    sudo apt-get install gnupg software-properties-common -y

    to_install=""
    if command_exists "yarn"; then
      echo "✔️ Yarn is already installed"
    else
      curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
      echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list

      to_install="${to_install} yarn"
    fi

    if command_exists "node"; then
      echo "✔️ Node.js is already installed"
    else
      curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -

      to_install="${to_install} nodejs"
    fi

    if command_exists "docker"; then
      echo "✔️ Docker is already installed"
    else
      curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
      sudo apt-key fingerprint 0EBFCD88
      sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"

      to_install="${to_install} apt-transport-https ca-certificates curl gnupg-agent software-properties-common docker-ce docker-ce-cli containerd.io"
    fi

    if command_exists "git"; then
      echo "✔️ Git is already installed"
    else
      to_install="${to_install} git"
    fi

    # installing the dependencies
    if [ -n "$to_install" ]; then
      sudo apt-get update && sudo apt-get install $to_install -y
    fi

    echo "✔ All the dependencies are installed"

    echo "✔ Cloning the repo"

    git clone https://gitlab.com/flusso/runner.git
    cd runner

    echo "✔ Installing the repo"

    yarn && yarn build

  fi
}

if [ "$EUID" -ne 0 ]; then
  echo "❗ Please run as root !!!"
  exit
fi

install

echo "✔ Done!"
echo "✔ Now you can run \"yarn register\" to register the runner!"
